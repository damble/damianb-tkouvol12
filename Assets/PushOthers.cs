using UnityEngine;
using System.Collections;

public class PushOthers : MonoBehaviour
{
	private Rigidbody2D _rigidBody;

	public Spawner  Father = null;
	// Use this for initialization
	void Start ()
	{
		_rigidBody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Father == null) {
			return;
		}
		float force = 0.7f;
		lock (Father.BallList) {
			foreach (var ball in Father.BallList) {
				if (ball == null) {
					continue;
				}
				Vector3 distance = transform.position - ball.transform.position;
				if (distance.magnitude < 3) {
					_rigidBody.AddForce (force * distance);
					ball.GetComponent<Rigidbody2D> ().AddForce (-force * distance);
				}
			}
		}
	}
}

