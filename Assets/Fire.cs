﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour
{
	[SerializeField]
	private GameObject
		_tower = null;
	
	private float _speed = 4f;
	private float _distance = 4f;
	
	private Vector3 _initPos = Vector3.zero;
	
	public Vector3 MoveVector = Vector3.zero;
	
	public GameObject Father = null; 
	
	
	// Use this for initialization
	void Start ()
	{
		_distance = Random.Range (1, 4);
		_initPos = transform.position;
		
	}
	
	// Update is called once per frame
	void Update ()
	{	
		if (DistanceTraveled ()) {
			if (TowersTracker.CanSpawnTower) {
				SpanwTower ();
			}
			Destroy (gameObject);
		} else {
			transform.position += MoveVector * _speed * Time.deltaTime;
		}
	}
	
	bool DistanceTraveled ()
	{
//		Debug.Log ((transform.position - _initPos).magnitude);
		return Mathf.Abs ((transform.position - _initPos).magnitude) >= _distance;
	}
	
	
	void SpanwTower ()
	{
		if (_tower == null) {
			return;
		}
		
		GameObject newTower = Instantiate (_tower);
		TurnAround turning = newTower.GetComponent<TurnAround> ();
		newTower.transform.position = transform.position;
		newTower.transform.rotation = Quaternion.identity;
		turning.WaitTime = 6f;
		
	}
	
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject != Father) {
			Destroy (gameObject);
			Destroy (other.gameObject);			
		}
	}
}
