﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowersTracker : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	[SerializeField]
	private Text
		_towersText = null;
	
	private bool _criticalAmountExeeded = false;
	
	private float _lastFrameTime = 0;
	
	
	static public bool CanSpawnTower = true;
	
	// Update is called once per frame
	void Update ()
	{
		//if (_criticalAmountExeeded) {
		//return;
		//}
		if (Time.unscaledTime - _lastFrameTime > 1) {
			_lastFrameTime = Time.unscaledTime;
			GameObject[] towers = GameObject.FindGameObjectsWithTag ("tower");
			if (towers.Length >= 100 && !_criticalAmountExeeded) {
				_criticalAmountExeeded = true;	
				UpdateTowers (towers);
				CanSpawnTower = false;
			}
			_towersText.text = towers.Length.ToString ();
		}
	}
	
	void UpdateTowers (GameObject[] towers)
	{
		foreach (var tower in towers) {
			tower.GetComponent<TurnAround> ().MakeMad ();
		}
	}
}
