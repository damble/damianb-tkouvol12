﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour
{

	public Rigidbody2D RigidBody;
	
	public  BoxCollider2D Collider;

	public Spawner Father = null;

	public int BallsInside = 1;

	private  float _beforeStartColissionTime = 0;

	private  float _physicsOffTime = 0;
	
	// Use this for initialization
	void Start ()
	{
		RigidBody = GetComponent<Rigidbody2D> ();
		Collider = GetComponent<BoxCollider2D> ();
		
		transform.localScale = transform.localScale * (float)(Mathf.Log10 (RigidBody.mass) + 1.1f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (BallsInside >= 50) {
			Father.Break (gameObject);
			Destroy (gameObject);
		}
		
		if (!Collider.enabled && Time.unscaledTime - _beforeStartColissionTime > _physicsOffTime) {
			//RigidBody.isKinematic = false;
			Collider.enabled = true;
			_beforeStartColissionTime = Time.unscaledTime;
		}
	}
	
	public void TurnOffPhysics (float time)
	{
		_physicsOffTime = time;
		//GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<BoxCollider2D> ().enabled = false;
		_beforeStartColissionTime = Time.unscaledTime;
	}
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if (Father == null) {
			return;
		}
		Collider.enabled = false;// isKinematic = true;
		other.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		Father.Merge (gameObject, other.gameObject);
		Destroy (gameObject);
		//Destroy (other.gameObject);
	}
}
