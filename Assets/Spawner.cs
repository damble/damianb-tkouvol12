﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{

	[SerializeField]
	private GameObject
		_modelBall = null;

	[SerializeField]
	private Text
		_ballCount = null;
	
	
	public List<GameObject> BallList = new List<GameObject> ();

	// Use this for initialization
	void Start ()
	{
		_textUpdateTime = _elapsedTime = Time.unscaledTime;
	}
	
	[SerializeField]
	private float
		_spawnFrequency = 0.25f;
	private float _elapsedTime = 0;
	
	private float _textUpdateTime;
	
	private bool _worldStateChanged = false;
	
	// Update is called once per frame
	void Update ()
	{
		PullBallsToCursor ();
		
		if (!_worldStateChanged && BallList.Count >= 250) {
			_worldStateChanged = true;
			ChangeWorldState ();
		}
		if (!_worldStateChanged && Time.unscaledTime - _elapsedTime >= _spawnFrequency) {
			_elapsedTime = Time.unscaledTime;
			SpawnBall (new Vector3 (Random.Range (-2, 2), Random.Range (-2, 2), 0));
		}
		
		if (Time.unscaledTime - _textUpdateTime > 1) {
			if (_ballCount == null) {
				return;
			}
			_ballCount.text = BallList.Count.ToString ();
			_textUpdateTime = Time.unscaledTime;
		}		
		
	}
	
	GameObject SpawnBall (Vector3 newPosition)
	{
		if (_modelBall == null) {
			return null;
		}
		GameObject newball = Instantiate (_modelBall);
		newball.transform.position = newPosition;
		BallBehaviour ballScript = newball.GetComponent<BallBehaviour> ();
		ballScript.Father = this;
		newball.SetActive (true);
		lock (BallList) {
			BallList.Add (newball);
		}
		return newball;
	}
	
	
	void PullBallsToCursor ()
	{
		Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		lock (BallList) {
			foreach (var ball in BallList) {
				if (ball == null) {
					continue;
				}
				ball.GetComponent<Rigidbody2D> ().AddForce ((mousePos - (Vector2)ball.transform.position) * 0.5f);
			}
		}
	}
	
	public void Merge (GameObject firstBall, GameObject secondBall)
	{
		if (_worldStateChanged) {
			return;
		}
		lock (BallList) {
			if (BallList.IndexOf (firstBall) < 0) {
				return;
			}
			BallList.Remove (firstBall);
			BallList.Remove (secondBall);
		
			GameObject newball = (GameObject)Instantiate (_modelBall, firstBall.transform.position, firstBall.transform.rotation);
			//newball.transform.position = (firstBall.transform.position + secondBall.transform.position) / 2;
			//newball.GetComponent<BallBehaviour> ().Father = this;
			//newball.SetActive (true);
			BallBehaviour behaviour = newball.GetComponent<BallBehaviour> ();
			behaviour.BallsInside = firstBall.GetComponent<BallBehaviour> ().BallsInside + secondBall.GetComponent<BallBehaviour> ().BallsInside;
			newball.GetComponent<Rigidbody2D> ().mass += firstBall.GetComponent<Rigidbody2D> ().mass + secondBall.GetComponent<Rigidbody2D> ().mass;
			//	behaviour.GetComponent<BoxCollider2D> ().enabled = false;//true;// RigidBody.isKinematic = false;
			
			newball.SetActive (true);
			BallList.Add (newball);
		}
		//	Destroy (firstBall);
		//	DestroyImmediate (secondBall);
	}
	
	public void Break (GameObject ball)
	{
		if (_worldStateChanged) {
			return;
		}
		lock (BallList) {
			BallList.Remove (ball);
		
			int forceToApply = 40;
			float offset = 0.15f;
			int ballsInside = ball.GetComponent<BallBehaviour> ().BallsInside;
			for (int i=0; i<ballsInside; i++) {
				Vector3 newPos = new Vector3 (ball.transform.position.x + Random.Range (-offset, offset), ball.transform.position.y + Random.Range (-offset, offset), 0);
				GameObject newBall = SpawnBall (newPos);
				Rigidbody2D ballBody = newBall.GetComponent<Rigidbody2D> ();
				newBall.GetComponent<BallBehaviour> ().TurnOffPhysics (0.5f);
				newBall.GetComponent<BallBehaviour> ().BallsInside = 1;
				ballBody.mass = 1;
				ballBody.AddForce (new Vector2 (Random.Range (-forceToApply, forceToApply), Random.Range (-forceToApply, forceToApply)));			
				BallList.Add (newBall);
			}
		}
		//	Destroy (ball);
	}
	
	
	void ChangeWorldState ()
	{
		lock (BallList) {
			foreach (var ball in BallList) {
				if (ball == null) {
					continue;
				}
				ball.GetComponent<Rigidbody2D> ().drag = 0.1f;
				PushOthers script = ball.AddComponent<PushOthers> ();
				script.Father = this;
			}
		}
	}
}
