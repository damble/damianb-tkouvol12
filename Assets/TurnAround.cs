﻿using UnityEngine;
using System.Collections;

public class TurnAround : MonoBehaviour
{
	[SerializeField]
	private GameObject
		_projectile = null;
		
	[SerializeField]
	public float
		WaitTime = 0;
	
	private bool _started = false;
	
	private float _initTimeBeforeStart = 0;
	
	// Use this for initialization	
	void Start ()
	{
		GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255);
		_lastSpinTime = Time.unscaledTime;
		_shotsFired = 0;
		_started = false;
		_initTimeBeforeStart = Time.unscaledTime;
	}
	
	private float _spinTime = 0.5f;//half a second
	private float _lastSpinTime = 0;
	
	private int _shotsFired = 0;
	//private int _shotsLimit=6;
		
		
	// Update is called once per frame
	void Update ()
	{
		if (Time.unscaledTime - _initTimeBeforeStart < WaitTime) {
			return;
		} else if (!_started) {
			GetComponent<SpriteRenderer> ().color = new Color (255, 0, 0);
		}
		
		if (_shotsFired < 12 && Time.unscaledTime - _lastSpinTime > _spinTime) {
			_lastSpinTime = Time.unscaledTime;
			SpinMe ();
			if (_projectile == null) {
				Debug.LogError ("no projectile");
				return;
			} else {
				FireProjectile ();
			}
		} else if (_shotsFired >= 12) {
			GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255);
		}
	
	}
	
	void SpinMe ()
	{
		var angle = Random.Range (15, 45);
		transform.Rotate (new Vector3 (0, 0, 1), angle);
	}
	
	void FireProjectile ()
	{
		GameObject newProjectile = Instantiate (_projectile);
		Fire firing = newProjectile.GetComponent<Fire> ();
		firing.MoveVector = transform.right + transform.up;
		firing.Father = gameObject;
		newProjectile.transform.position = transform.position;
		newProjectile.SetActive (true);		
		_shotsFired++;
	}
	
	public void MakeMad ()
	{
		_shotsFired = 0;
		GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255);
	}
}
